package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          mergeSort(lista, orden);
          return lista;
     }


     private static Comparable[] mergeSort(Comparable[] list, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
    	//If list is empty; no need to do anything
         if (list.length <= 1) {
             return list;
         }
          
         //Split the array in half in two parts
         Comparable[] first = new Comparable[list.length / 2];
         Comparable[] second = new Comparable[list.length - first.length];
         System.arraycopy(list, 0, first, 0, first.length);
         System.arraycopy(list, first.length, second, 0, second.length);
          
         //Sort each half recursively
         mergeSort(first, orden);
         mergeSort(second, orden);
          
         //Merge both halves together, overwriting to original array
         merge(first, second, list, orden);
         return list;
     }

     private static void merge(Comparable[] first, Comparable[] second, Comparable[] result, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase 
    	//Index Position in first array - starting with first element
         int iFirst = 0;
          
         //Index Position in second array - starting with first element
         int iSecond = 0;
          
         //Index Position in merged array - starting with first position
         int iMerged = 0;
          
         //Compare elements at iFirst and iSecond, 
         //and move smaller element at iMerged
         while (iFirst < first.length && iSecond < second.length) 
         {
             if (first[iFirst].compareTo(second[iSecond]) < 0) 
             {
                 result[iMerged] = first[iFirst];
                 iFirst++;
             } 
             else
             {
                 result[iMerged] = second[iSecond];
                 iSecond++;
             }
             iMerged++;
         }
         //copy remaining elements from both halves - each half will have already sorted elements
         System.arraycopy(first, iFirst, result, iMerged, first.length - iFirst);
         System.arraycopy(second, iSecond, result, iMerged, second.length - iSecond);
     }
}



