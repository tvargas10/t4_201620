package taller.mundo.teams;

/*

* QuickSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;

import java.util.Random;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class QuickSortTeam extends AlgorithmTeam
{

     private static Random random = new Random();

     public QuickSortTeam()
     {
          super("Quicksort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          quicksort(list, 0, list.length-1, orden);
          return list;
     }
        // Trabajo en Clase
     
     private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
     {
    // Trabajo en Clase
    if (inicio < fin)
         {
             /* pi is partitioning index, arr[pi] is 
               now at right place */
             int pi = particion(lista, inicio, fin,orden);
  
             // Recursively sort elements before
             // partition and after partition
             quicksort(lista, inicio, pi-1,orden);
             quicksort(lista, pi+1, fin,orden);
         }
     }

    private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
    {
    // Trabajo en Clase
    if(orden.equals(TipoOrdenamiento.ASCENDENTE))
    {
    Comparable pivot = lista[fin]; 
    int i = (inicio-1); // index of smaller element
    for (int j=inicio; j<=fin-1; j++)
    {
    // If current element is smaller than or
    // equal to pivot
    if (lista[j].compareTo(pivot) < 0)
    {
    i++;

    // swap arr[i] and arr[j]
    Comparable temp = lista[i];
    lista[i] = lista[j];
    lista[j] = temp;
    }
    }

    // swap arr[i+1] and arr[high] (or pivot)
    Comparable temp = lista[i+1];
    lista[i+1] = lista[fin];
    lista[fin] = temp;

    return i+1;
    }
    else
    {
    Comparable pivot = lista[fin]; 
    int i = (inicio-1); // index of smaller element
    for (int j=inicio; j<=fin-1; j++)
    {
    // If current element is smaller than or
    // equal to pivot
    if (lista[j].compareTo(pivot) > 0)
    {
    i++;

    // swap arr[i] and arr[j]
    Comparable temp = lista[i];
    lista[i] = lista[j];
    lista[j] = temp;
    }
    }

    // swap arr[i+1] and arr[high] (or pivot)
    Comparable temp = lista[i+1];
    lista[i+1] = lista[fin];
    lista[fin] = temp;

    return i+1;
    }
    }

    private static int eleccionPivote(int inicio, int fin)
    {
         /**
           Este procedimiento realiza la eleccion de un indice que corresponde al pivote res-
           pecto al cual se realizar  la particion de la lista. Se recomienda escoger el ele-
           mento que se encuentra en la mitad, o de forma aleatoria entre los indices [inicio, fin).
         **/
    // Trabajo en Clase
    
         return 0;
    }

    /**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
    **/
    public static int randInt(int min, int max) 
    {
          int randomNum = random.nextInt((max - min) + 1) + min;
          return randomNum;
    }
    // Trabajo en Clase

}



